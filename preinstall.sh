#!/bin/sh

mv node_modules/backgrid/lib/backgrid.css node_modules/backgrid/lib/backgrid.scss
mv node_modules/select2/select2.css node_modules/select2/select2.scss
mv node_modules/select2/select2-bootstrap.css node_modules/select2/select2-bootstrap.scss
cp -r node_modules/bootstrap-sass/assets/fonts/ dist/
cp model/config/config.json.dist model/config/config.json

