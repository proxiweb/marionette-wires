var restify = require('restify');

var authRules = {};


authRules.isOwnerOrAdmin = function(req, res, next) {
	if ( typeof req.user === 'undefined' || 
		(parseInt(req.user.id) !== parseInt(req.params.id) && !req.user.superAdmin)) {
			res.send(new restify.UnauthorizedError('accès non autorisé '));
	} else {
		return next();		
	}
};

authRules.isUser = function(req, res, next) {
	if ( typeof req.user === 'undefined' ) {
			res.send(new restify.UnauthorizedError('accès non autorisé '));
	} else {
		return next();		
	}
};

authRules.isAdmin = function (req, res, next) {
	if (typeof req.user === 'undefined' || !req.user.superAdmin) {
		res.send(new restify.UnauthorizedError('accès non autorisé'));
	} else {
		return next();		
	}
};

authRules.forAll = function (req, res, next) {
	return next();
};



module.exports = authRules;