"use strict";
module.exports = function(sequelize, DataTypes) {
  var Utilisateurs = sequelize.define("Utilisateur", {
    pseudo: DataTypes.STRING,
    motDePasse: DataTypes.STRING,
    email: DataTypes.STRING,
    actif: DataTypes.BOOLEAN,
    superAdmin: DataTypes.BOOLEAN,
    lastLogin: DataTypes.DATE,
    quota: DataTypes.INTEGER,
    apiKey: DataTypes.STRING,
  }, {
    tableName: 'utilisateurs',
    classMethods: {
      associate: function(models) {
        Utilisateurs.hasMany(models.sentitem, { foreignKey: 'UtilisateurId' });
        Utilisateurs.hasMany(models.outbox, { foreignKey: 'UtilisateurId' });
        Utilisateurs.hasMany(models.utilisateur_sms, { foreignKey: 'UtilisateurId' });              
      }
    }
  });
  return Utilisateurs;
};
