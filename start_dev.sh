#!/bin/sh

forever start -c nodemon bin/api.js
forever start -c nodemon bin/app.js
forever start -c nodemon bin/proxy.js
