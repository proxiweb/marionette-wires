import ModelBinder from 'modelbinder'; /*jshint unused: false*/
import validation from 'backbone-validation';
import Layoutview from '../common/layout-view';
import Radio from 'backbone.radio';
import Backbone from 'backbone';

export default Layoutview.extend({

  tagName: 'form',

  events: {
    'submit': 'submit',
    'click .btn-default': 'cancel',
    'click .close': 'cancel'
  },

  initialize() {
    this.modelBinder = new Backbone.ModelBinder();
    Backbone.Validation.bind(this);
    Radio.request('modal', 'open', this);
  },

  onViewModalified() {
    //var bindings = this.bindings || {};
    this.modelBinder.bind(this.model, this.el);

    if (this.options.title) {
      this.$('.modal-title').html(this.options.title);
    }

    if (this.options.submitLabel) {
      this.$('button[type="submit"]').html(this.options.submitLabel);
    }

    this.triggerMethod('model:binded');
  },

  onModelBinded() {},

  submit(e) {
    e.preventDefault();
    if (this.model.isValid(true)) {
      this.model.save({}, {
        success: function (ville, response, options) {
          Radio.request('modal', 'close');
        }
      });
    }
  },

  onDestroy() {
    this.modelBinder.unbind();
    Backbone.Validation.unbind(this);
  },


  cancel() {
    Radio.request('modal', 'close').then(() => {
      this.trigger('cancel');
    });
  }
});
