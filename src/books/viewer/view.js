import View from '../../common/view';
import template from './template.hbs';
import toastr from 'toastr';

export default View.extend({
  template: template,
  modelEvents: {
    'all': 'render'
  },
  onAttach() {
  	this.editor = CKEDITOR.replace('test',{
        contentsCss: [CKEDITOR.basePath + 'contents.css', '/bundle.css'],
        allowedContent: true,
  	});
  	this.editor.on('save',e => {
  		toastr["success"]('Sauvegarde effectuée');
  	});
  }
});
