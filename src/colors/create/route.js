import Route from '../../common/route';
import Model from '../model.js';
import View from './view';
import storage from '../storage.js';

export default Route.extend({
  initialize(options) {
    this.container = options.container;
    this.collection = options.collection;
  },

  fetch() {
    return storage.findAll().then(collection => {
      this.collection = collection;
    });
  },

  render() {
    this.model = new Model();
    this.view = new View({
      collection: this.collection,
      model: this.model
    });
    this.container.show(this.view);
  }
});
