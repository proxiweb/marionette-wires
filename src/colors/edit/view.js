import Backbone from 'backbone';
import FormBehavior from '../../forms/behavior';
import _ from 'lodash';
import View from '../../common/view';
import template from './template.hbs';

export default View.extend({
  template: template,

  className: 'colors colors--edit container',

  behaviors: {
    form: {behaviorClass: FormBehavior}
  },

  templateHelpers() {
    return {
      errors: this.model.validationError
    };
  },

  initialize() {
    _.bindAll(this, 'handleSaveSuccess');
  },

  events: {
    'submit form': 'handleSubmit'
  },

  handleSubmit() {
    var errors = this.model.validate(this.form);

    if (!errors) {
      this.model
        .save(this.form)
        .done(this.handleSaveSuccess);
    } else {
      this.model.validationError = errors;
      this.render();
    }
  },

  handleSaveSuccess() {
    Backbone.history.navigate('colors/' + this.model.id, {trigger: true});
  }
});
