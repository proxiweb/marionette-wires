import Backbone from 'backbone';

export default Backbone.Collection.extend({
  constructor() {
    Backbone.Collection.apply(this,arguments);
    this._isNew = true;
    this.once('sync', () => {
      this._isNew = false;
    });
  },

  isNew() {
    return this._isNew;
  }
});
