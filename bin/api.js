var restify = require('restify');
var server = restify.createServer();
var models = require('../model/models');

//models.sequelize.sync();

server.use(restify.fullResponse())
  .use(restify.bodyParser())
  .use(restify.queryParser());

server.pre(function (request, response, next) {
  models.Utilisateur.find({where: {apiKey: request.header('X-Api-Key')}}).then(function(Utilisateur) {
	if (!Utilisateur) {
	  request.user = undefined;
      //return next(new restify.UnauthorizedError('utilisateur non trouvé'));
    } else {
		request.user = {
			id: Utilisateur.id,
			pseudo: Utilisateur.pseudo,
			superAdmin: Utilisateur.superAdmin
		};
    	return next();
    }
  }).error(function(err) {
      console.log('Erreur récupération utilisateur par apiKey', err);
      response.send(500);
      return next(); 
  });
});


require('../api/routes/sms')(server);
require('../api/routes/automatique')(server);


server.listen(8080, function() {
  console.log('%s listening at %s...', server.name, server.url);
});
