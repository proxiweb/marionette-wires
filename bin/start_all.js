var forever = require('forever-monitor');

var api = new (forever.Monitor)('api.js', {
  max: 3,
  silent: true,
  options: []
});

var proxy = new (forever.Monitor)('proxy.js', {
  max: 3,
  silent: true,
  options: []
});

var app = new (forever.Monitor)('api.js', {
  max: 3,
  silent: true,
  options: []
});

//child.on('exit', function () {
//  console.log('your-filename.js has exited after 3 restarts');
//});

api.start();
proxy.start();
app.start();

