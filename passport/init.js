var login = require('./login');
var signup = require('./signup');
var Models = require('../model/models/index');

module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(utilisateur, done) {
        done(null, utilisateur.id);
    });

    passport.deserializeUser(function(id, done) {
        Models.Utilisateur.find({where: {id: id}}).then(function(utilisateur){
            done(null, utilisateur);
        }).error(function(err){
            done(err, null);
        });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport);
    signup(passport);

};
